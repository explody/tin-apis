# Tin API files

Herein is the growing body of API definition files for use with
[Tin](https://gitlab.com/explody/tin).

* `<vendor>/<vendor>.yml` - Basic settings such as auth type, hostname, paths
  to api and models files, etc.
* `<vendor>/<vendor>-api.yml` - Definition of endpoints, methods, expected
  returns and so on
* `<vendor>/<vendor>-models.yml` - Config for models which may be optionally
  used in the API definitions.

## Currently Supported Vendors

This is a very short list as of Sept '21.  A vendor appearing here certainly
does not mean their REST API is 100% defined, but it is very easy to add
additional API classes and methods.

* **Freshservice** - The most complete of the three currently, but only about
  25% there.
* **Kace** - Rudiments
* **Servicenow** - Rudiments, likely will go stale as I've moved away from SNow
