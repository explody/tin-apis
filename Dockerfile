FROM nikolaik/python-nodejs:python3.9-nodejs16-alpine as tin-apis

RUN apk add --update-cache --no-cache git && \
    npm install semantic-release @semantic-release/gitlab @semantic-release/changelog @semantic-release/git conventional-changelog-eslint markdownlint -g && \
    pip install yamllint && \
    pip cache purge && \
    npm cache clean --force

