## [1.4.1](https://gitlab.com/explody/tin-apis/compare/v1.4.0...v1.4.1) (2024-03-28)


### Fix

* return codes for a couple FS endpoints ([951f3ce](https://gitlab.com/explody/tin-apis/commit/951f3ceb937234f1fbbfa5c4f02d0c75d9ca3543))

# [1.4.0](https://gitlab.com/explody/tin-apis/compare/v1.3.1...v1.4.0) (2023-10-06)


### New

* basics for Insperity ([3730817](https://gitlab.com/explody/tin-apis/commit/37308171ac50974e40c25143735b82bbed9652ed))

## [1.3.1](https://gitlab.com/explody/tin-apis/compare/v1.3.0...v1.3.1) (2023-10-05)


### Fix

* fs department get should be singleton ([156be85](https://gitlab.com/explody/tin-apis/commit/156be85641f27ebc3c2e01f9ba70f9e5322925ad))

# [1.3.0](https://gitlab.com/explody/tin-apis/compare/v1.2.0...v1.3.0) (2023-09-27)


### New

* asset deletion for FS, avatar upload for Bob ([0f38e2d](https://gitlab.com/explody/tin-apis/commit/0f38e2d8e502d94172b98d531c6b87d7c8214ec7))

# [1.2.0](https://gitlab.com/explody/tin-apis/compare/v1.1.0...v1.2.0) (2023-06-14)


### New

* response_data_key for various endpoints ([95a1857](https://gitlab.com/explody/tin-apis/commit/95a18574a2e77e7e989757163be870d320beeca0))

# [1.1.0](https://gitlab.com/explody/tin-apis/compare/v1.0.3...v1.1.0) (2023-06-14)


### New

* a bunch more ticket endpoints ([f934bfe](https://gitlab.com/explody/tin-apis/commit/f934bfedb545b0d031d7d30bc9795727aaa28218))

## [1.0.3](https://gitlab.com/explody/tin-apis/compare/v1.0.2...v1.0.3) (2023-03-08)


### Fix

* A couple more response_data_key ([e005fdf](https://gitlab.com/explody/tin-apis/commit/e005fdf888bdf142e4e66ad85a5d3892cf1c5baf))

## [1.0.2](https://gitlab.com/explody/tin-apis/compare/v1.0.1...v1.0.2) (2023-03-08)


### Fix

* updating/fixing Freshservice methods to handle their responses ([06c0f4a](https://gitlab.com/explody/tin-apis/commit/06c0f4a66a6a0b0873c6725d56cb9e97690cefcf))

## [1.0.1](https://gitlab.com/explody/tin-apis/compare/v1.0.0...v1.0.1) (2023-03-08)


### Fix

* asset_types was a copy+paste. Update it to match the actual API ([867d855](https://gitlab.com/explody/tin-apis/commit/867d8555e429b3689a395bce81c166df88843c77))

# 1.0.0 (2023-03-01)


### Breaking

* update config schema following tin config refactor ([c898e20](https://gitlab.com/explody/tin-apis/commit/c898e20b781fdb5cb9c1897b9e21580b9ea96f0f))

### Fix

* a couple return codes ([dcee95c](https://gitlab.com/explody/tin-apis/commit/dcee95cdf8125b3199a265ec8c9b1a6efef14f2d))
* update hibob api file ([6bf2372](https://gitlab.com/explody/tin-apis/commit/6bf2372e4f1dd42bea4f9899b0e2ab3dd5311b0e))
* update hibob api file ([d9b08e5](https://gitlab.com/explody/tin-apis/commit/d9b08e52a0bca0c2b5393847d87367273dedb00b))
* zeroing out prior release versions and starting clean ([f10c2f3](https://gitlab.com/explody/tin-apis/commit/f10c2f3b404056f1b4e9519a9f9564caf51d53b9))

### New

* extremely basic Okta support ([9e05f64](https://gitlab.com/explody/tin-apis/commit/9e05f64a9aa9a483934c700fd9c823bba4316218))
* fairly large update to the FS api ([6e0883e](https://gitlab.com/explody/tin-apis/commit/6e0883efa84e6748107fd5f17478574047f52482))
* freshservice custom objects, Okta apps ([8c1df8e](https://gitlab.com/explody/tin-apis/commit/8c1df8ecfe6e23cce2610e482ddd1c8201d26e45))
* freshservice locations and onboarding_requests ([e19c5d6](https://gitlab.com/explody/tin-apis/commit/e19c5d6b5376bbab2cf8d9f43aa901f75998efb4))
* Replies and conversations for Freshservice ([aec468e](https://gitlab.com/explody/tin-apis/commit/aec468e556caca2a5a1c6fa5ae40b34f9d060831))
* rudiments of miro, ADP, hibob ([41eee26](https://gitlab.com/explody/tin-apis/commit/41eee26ccee948a1394590d5e2bd5657e1fb4343))
